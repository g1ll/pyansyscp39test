### Using PyAnsys to Solve a Double T-Shaped Cooled Cavity
<img title="Duplo-T" src="results/t_cavit_result_20220424-124227_026518.png" width="250"  />
<img title="Duplo-T" src="results/t_cavit_result_20220424-124753_863707.png" width="250"  />
<img title="Duplo-T" src="results/t_cavit_result_20220427-235248_835836.png" width="250"  />

>[es]

### Install Project

to install dependences use virtualenv with python 3.9:

```powershell
cd pyansyscp39test-main/
virtualenv.exe . -p C:\Users\{YOUR_USER_NAME}\AppData\Local\Programs\Python\Python39\python.exe
```

activate the env
```powershell 
Scrtipts/activate.ps1
```
verify python version

```powershell
(pyansyscp39test-main)>python --version
Python 3.9
```
and run pip to install all dependences:

```powershell
pip install -r requirements.txt
```
### Run Example

after install dependences run an example using

```powershell
python 2tshaped_plot.py
```
>[pt-br]

### Instalação

Compatível com ambiente Windows e Ansys versão >= R16 instalado com suporte ao MAPDL.

Testado na versão Student do Ansys.

Instale o Python para windows, padrão ultima versão [3.10.4](https://www.python.org/downloads/release/python-3104/).

O Python e o pip (gerenciador de pacotes para python) devem estar funcionais no terminal.

Caso seja questionado, confirme a adição no PATH do sistema durante a instalação.

Teste no terminal (para abrir o prompt: tecla super[logo do window]+x i)

```powershell
python --version
Python 3.10.4

pip --version
pip 22.0.4
```
Instale o Python 3.9 para Windows e compatível com as bibliotecas do repositório, sem colocá-lo no PATH do sistema.

[Link](https://www.python.org/downloads/release/python-3912/)

Preste atenção ou anote o diretório de instalação do python3.9 no windows

Normalmente é instalado na pasta C:\Users\NomeDoSeuUsuaio\appData\Local\Programs\Python\

Instale o virtualenv

```powershell
pip install virtualenv
```

Faça download dos aquivos do repositório

Extraia tudo em uma pasta

Abra o diretório no PowerShell (super+x i) ou shift+[direito do mouse\] e abrir terminal.

No terminal do PowerShell entre na pasta do projeto

```powershell
cd pyansyscp39test-main/
```
Inicialize o ambiente virtual com o virtualenv.

No lugar de {YOUR_USER_NAME} use o nome da dua pasta de usuário no windows.

```powershell
virtualenv.exe . -p C:\Users\{YOUR_USER_NAME}\AppData\Local\Programs\Python\Python39\python.exe
```

O virtualenv criara as pastas ***Scripts*** e ***Lib*** dentro do diretório do projeto com a versão do python apropriada, neste caso o 3.9, assim como os scripts de ativação do ambiente virutal python. A pasta *Lib* guardará os pacotes que serão instalados nos passos a seguir.

Ative o ambiente virtual com o script *activate.ps1* para PowerShell localizado na pasta Scripts:

```powershell
.\Scrtipts\activate.ps1
```

Deverá aparecer o nome da pasta antes do terminal de comandos, sinalizando o ambiente virtual.

Verifique a versão do python.

```powershell
PS (pyansyscp39test-main) > python --version
Python 3.9.12
```
Tem que ser a 3.9!

Execute o pip para instalar as dependencias apartir do arquivo requirements.txt:

```powershell
python -m pip install -r requirements.txt
```
### Execute um exemplo para a cavidade duplo-t

Após instalar todas as dependências

```powershell
python 2tshaped_plot.py
```

/Deverá resolver o problema da cavidade Duplo-T plotando a construção da geometria, malha e campo de temperaturas

Na pasta *results* são salvas as figuras geradas.

![alt exemplo](results/t_cavit_result_20220427-235248_835836.png "Duplo-T")

### References

[PyAnsys MAPDL Official Documentation](https://mapdldocs.pyansys.com/)

[PyAnsys MAPDL Official Repository](https://github.com/pyansys/pymapdl/)

[Double T-shaped Cavity Problem](https://doi.org/10.1016/j.ijheatmasstransfer.2021.121268)

[Python Virtualenv Documentation](https://virtualenv.pypa.io/en/latest/)

[VTK Library](https://vtk.org/download/)
