from ansys.mapdl.core import launch_mapdl
from pyvista import themes
import datetime

mapdl = launch_mapdl()

mapdl.clear()
mapdl.prep7()

# mapdl.et(1,'SURF151')
mapdl.et(1, 'PLANE35')  # tipo de elemento - no triangular

# Propriedados do material
mapdl.units("SI")  # Sistema SI

mapdl.mp("KXX", 1, 1)  # condutividade do material
mapdl.mp("C", 1, 1)  # calor específico

body = mapdl.blc4(-0.5, 0, 1, 1)
trunk = mapdl.blc4(-0.1, 0, 0.2, 0.6)
body_trunk = mapdl.asba(body, trunk)
branch = mapdl.blc4(-0.3, 0.6, 0.6, 0.2)
cavity_t = mapdl.asba(body_trunk, branch)
# Plotando as linhas da geometria
mapdl.lplot(
    line_width=3,
    cpos="xy",
    color_lines=True,
    background="w"
)

# Plotanto o domínio
mapdl.aplot(
    show_lines=True,
    line_width=3,
    show_bounds=True,
    cpos="xy",
    background="w",
    show_area_numbering=True
)

# Criação da malha
mapdl.esize(0.01)
mapdl.amesh(cavity_t)

# Plotando a malha e seus elementos triangulares (PLANE35)
mapdl.eplot(
    vtk=True,
    cpos="xy",
    show_edges=True,
    show_axes=False,
    line_width=2,
    background="w"
)

# Condições de contorno
# Geração de calor interno e fluxo constante
mapdl.asel('S', vmin=0, vmax=2)  # Seleção da area do dominio
mapdl.nsla()  # selecao de nos adjacentes a area
mapdl.bf('all', 'HGEN', 1)  # aplicação  da geracao de calor
mapdl.sf('all', 'HEAT', 1)  # efinicao do fluxo

# Condições de Dirichlet da cavidade
mapdl.lsel("S", vmin=5, vmax=8)  # selecao da laterais da cavidade
mapdl.lsel("A", vmin=11, vmax=14)  # selecao das laterais da cavidade
mapdl.nsll('S', 1)  # selecao dos nos adjacentes
mapdl.d("all", "TEMP", 0)  # aplicacao da condicao contorno

# Condição de Neumann
mapdl.lsel("S", vmin=2, vmax=4)  # selecao de linhas alterais do solido
mapdl.lsel("A", vmin=9, vmax=10)  # selecao de linhas laterais do solido
mapdl.nsll('S', 0)  # selecao de nos adjacentes
mapdl.sf("all", "HFLUX", 0)  # aplicacao da condicao de fluxo nulo

out = mapdl.allsel()  # selecao de todos os elementos para o solver

# solve
# mapdl.antype(0)
mapdl.run("/SOLU")
print(mapdl.solve())

result = mapdl.result
nnum, temp = result.nodal_temperature(0)
# this is the same as pyansys.read_binary(mapdl._result_file)
print(nnum, temp)
print(f"\nMax Temp:{max(temp)}\nMin Temp:{min(temp)}")

out = mapdl.finish()

temp_theme = themes.DocumentTheme()
temp_theme.cmap = 'jet'

mapdl.post1()
mapdl.set(1, 1)
# mapdl.post_processing.plot_nodal_temperature()
mapdl.post_processing.plot_nodal_temperature(cpos='XY',  theme=temp_theme)
filenamepng = f"results/t_cavit_result_{datetime.datetime.now().strftime('%Y%m%d-%H%M%S_%f')}.png"
print(filenamepng)
mapdl.post_processing.plot_nodal_temperature(
    cpos='XY',  theme=temp_theme, off_screen=True, savefig=filenamepng)

mapdl.exit()
