from ansys.mapdl.core import launch_mapdl

mapdl = launch_mapdl()

mapdl.clear()
mapdl.prep7()

#Propriedados do material
mapdl.units("SI")  # Sistema SI

mapdl.mp("KXX", 1, 1) #condutividade do material
mapdl.mp("C", 1, 1) # calor específico
mapdl.et(1,'PLANE35') #tipo de elemento - no triangular

body = mapdl.blc4(-0.5, 0,1, 1)
trunk = mapdl.blc4(-0.15, 0,0.3,0.6)
cavity_i =  mapdl.asba(body,trunk)
mapdl.aplot(show_lines=True, line_width=5, show_bounds=True, cpos="xy")

plate_esize = 0.01

# Decrease the area mesh expansion.  This ensures that the mesh
# remains fine nearby the hole
mapdl.mopt("EXPND", 0.7)  # default 1

mapdl.esize(plate_esize)
mapdl.amesh(cavity_i)
mapdl.eplot(
    vtk=True, cpos="xy", show_edges=True, show_axes=False, line_width=2, background="w"
)

#Condições de contorno



