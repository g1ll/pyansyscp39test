from ansys.mapdl.core import launch_mapdl
from timeit import default_timer as timer
import datetime
import math as m



mapdl = launch_mapdl()
mapdl.clear()

#Conta o tempo do processo
start = timer()

# DEFINICAO DE RESTRICOES E GRAUS DE LIBERDADE
# Restricoes de areas
fi1 = 0.015
fi2 = fi1
fi3 = 0.1 - 2.0*fi1-2.0*fi2

#Refino da Malha
elementSize = 1/100

# Configuracao otima da Tese para 4 dofs e (tmax)4m
#DOI:10.1016/j.ijheatmasstransfer.2021.121268
HdL = 1
H0dL0 = 16.483148
S1dH0 = 0.32350359
H1dL1 = 0.067797
H2dL2 = H1dL1

# Calculo das variaveis da geometria
L = m.sqrt(1/HdL)
H = HdL*L

l0 = m.sqrt(fi3/H0dL0)
h0 = H0dL0*l0

l1 = m.sqrt(fi1/(H1dL1))
h1 = H1dL1*l1

l2 = m.sqrt(fi2/(H2dL2))
h2 = H2dL2*l2

s1 = S1dH0*h0

#Definicao de pontos para desenho da geometria 
#stem
x_s = -l0/2
y_s = 0
w_s = l0
h_s = h0

#branch_inf
x_b1 = -(l1+l0/2)
y_b1 = s1-h1/2
w_b1 = 2*l1+l0
h_b1 = h1

#branch_sup
x_b2 = -(l2+l0/2)
y_b2 = h0-h2
w_b2 = 2*l2+l0
h_b2 = h2

mapdl.prep7()

#Definicao do tipo de elemento - nó triangular
mapdl.et(1,'PLANE35') 
#Sistema métrico SI
mapdl.units("SI") 
#Propriedados do material
mapdl.mp("KXX", 1, 1) #condutividade do material
mapdl.mp("KYY", 1, 1) #condutividade do material
mapdl.mp("C", 1, 1) # calor específico

#Construcao da geometria da Cavidade Duplo-t
body = mapdl.blc4(-L/2,0,L,H)               #body
stem = mapdl.blc4(x_s,y_s,w_s,h_s)          #stem
body_stem = mapdl.asba(body, stem)          #body-stem
branch1 = mapdl.blc4(x_b1,y_b1,w_b1,h_b1)   #inferior branch
branch2 = mapdl.blc4(x_b2,y_b2,w_b2,h_b2)   #superior branch
cavity_t =  mapdl.asba(body_stem, branch1)
cavity_2t =  mapdl.asba(cavity_t, branch2)


#Criação da malha
mapdl.esize(elementSize)
mapdl.amesh(cavity_2t)

#Condições de contorno
#Geracao de calor e fluxo na area do dominio
mapdl.asel('S',vmin=0,vmax=2)
mapdl.nsla()
mapdl.bf('all','HGEN',1)
mapdl.sf('all','HEAT',1)

#Temperatura mínima prescrita na cavidade
mapdl.lsel("S", vmin=1)
mapdl.lsel("A", vmin=5,vmax=8)
mapdl.lsel("A", vmin=11,vmax=26)
mapdl.nsll('S',1)
mapdl.d("all", "TEMP", 0)

#Fluxo nulo nas laterais do dominio
mapdl.lsel("S", vmin=2,vmax=4)
mapdl.lsel("A", vmin=9,vmax=10)
mapdl.nsll('S',1)
mapdl.sf("all", "HFLUX", 0)

#Selecao de todos elementos para o solver
out = mapdl.allsel()

#solve
# mapdl.antype(0)
mapdl.run("/SOLU")
print(mapdl.solve())



result = mapdl.result
nnum, temp = result.nodal_temperature(0)
# this is the same as pyansys.read_binary(mapdl._result_file)

print(f"\nMax Temp:{max(temp)}\n")

#tempo total do solver
end = timer()
print(f"Time: {end - start}") 

out = mapdl.finish()

mapdl.exit()
exit()