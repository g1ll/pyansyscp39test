from ansys.mapdl.core import launch_mapdl

mapdl = launch_mapdl()

mapdl.clear()
mapdl.prep7()

# mapdl.et(1,'SURF151')
mapdl.et(1,'PLANE35') #tipo de elemento - no triangular

#Propriedados do material
mapdl.units("SI")  # Sistema SI

mapdl.mp("KXX", 1, 1) #condutividade do material
mapdl.mp("C", 1, 1) # calor específico

body = mapdl.blc4(-0.5, 0,1, 1)
trunk = mapdl.blc4(-0.1, 0,0.2,0.6)
body_trunk = mapdl.asba(body, trunk)
branch = mapdl.blc4(-0.3,0.6,0.6,0.2)
cavity_t =  mapdl.asba(body_trunk, branch)

#Criação da malha
mapdl.esize(0.01)
mapdl.amesh(cavity_t)

#Condições de contorno
#Geração de calor interno e fluxo constante
mapdl.asel('S',vmin=0,vmax=2)       #Seleção da area do dominio
mapdl.nsla()                        #selecao de nos adjacentes a area
mapdl.bf('all','HGEN',1)            #aplicação  da geracao de calor 
mapdl.sf('all','HEAT',1)            #efinicao do fluxo

#Condições de Dirichlet da cavidade
mapdl.lsel("S", vmin=5,vmax=8)      #selecao da laterais da cavidade
mapdl.lsel("A", vmin=11,vmax=14)    #selecao das laterais da cavidade
mapdl.nsll('S',1)                   #selecao dos nos adjacentes
mapdl.d("all", "TEMP", 0)           #aplicacao da condicao contorno

#Condição de Neumann
mapdl.lsel("S", vmin=2,vmax=4)      #selecao de linhas alterais do solido
mapdl.lsel("A", vmin=9,vmax=10)     #selecao de linhas laterais do solido
mapdl.nsll('S',0)                   #selecao de nos adjacentes
mapdl.sf("all", "HFLUX", 0)         #aplicacao da condicao de fluxo nulo

out = mapdl.allsel()                #selecao de todos os elementos para o solver


out = mapdl.allsel()

#solve
# mapdl.antype(0)
mapdl.run("/SOLU")
mapdl.solve()

result = mapdl.result
nnum, temp = result.nodal_temperature(0)
print(f"\nMax Temp:{max(temp)}\n")

out = mapdl.finish()

mapdl.exit()